#include <systemc.h>
#include "memory.hpp"
#include <string.h>

void Memory::load_file() {
	int i,j;
	char c;
	FILE *inFilePtr;
	char tmp[DATA_WIDTH];
	char inFileString[15];
	
	wait(); //attendo inizializzazione variabile file
	sprintf(inFileString,"memory%d.txt",file->read());
	inFilePtr = fopen(inFileString, "r");
	
	if (inFilePtr == NULL) {
		cout << "error in opening " <<  inFileString << "\n";
		exit (EXIT_FAILURE);
	} else {
		i=1;
		j=DATA_WIDTH-1;
		data[0]=0;
		data[i] = 0;
		do {
			c = fgetc (inFilePtr);
			if (c == '\n' && j == -1) {
				j=DATA_WIDTH-1;
				i++;
			}
			else if (c != EOF && c !=' ') {
				tmp[j]=c;
				data[i].range(j,j) = c;
				j--;
			}
		} while (c != EOF);
		fclose(inFilePtr);
		
		//inizializza il resto della memoria a zero
		while(i <= DEPTH) {
			data[i] = 0;
			i++;
		}
		first_load.notify(SC_ZERO_TIME);
	}
}

void Memory::update_ports() {
	wait(first_load);
  while(true) {
  	wait(5,SC_NS);
  	if (cs->read() == 1 & we->read() == 1) {

  	}
  	
  	//Lettura:
  	if (cs->read() == 1 & oe->read() == 1) {
			dout->write(data[add->read().to_uint()]);  
		}
		
		//Scrittura:
		if (cs->read() == 1 & we->read() == 1) {
			data[add->read().to_uint()] = din->read();
			
			//salva contenuto della memoria in un file di testo	
			int i;
			std::string input;
			char inFileString[15];
			sprintf(inFileString,"memory%d_end.txt",file->read());
    	std::ofstream out(inFileString);

			i=0;
			do {
				input = data[i].to_string();
				out << input;				
				out << "\n";
				i++;
			} while (i <= DEPTH);
    	out.close();
    	//------------------------------------------------------		
		}
	wait();
	}
}
