#ifndef MEMORY_HPP
#define MEMORY_HPP

SC_MODULE(Memory) {
	static const unsigned ADDR_WIDTH = 16;
	static const unsigned DATA_WIDTH = 16;
	static const unsigned DEPTH = 1 << 16;
	
	sc_in<sc_lv<ADDR_WIDTH> > add;
	sc_in<sc_lv<DATA_WIDTH> > din;
	sc_out<sc_lv<DATA_WIDTH> >dout;
	sc_in<bool> cs; //chip select
	sc_in<bool> we; //write enable
	sc_in<bool> oe; //output enable
	sc_in<unsigned> file; //indice per il file della memoria
	
	sc_lv<DATA_WIDTH> data[DEPTH+1];
	sc_event first_load;
	
	SC_CTOR(Memory) {
		SC_THREAD(load_file);
			sensitive << file;
		SC_THREAD(update_ports);
			sensitive << we  << add << din << cs << oe;
	}
	
	private:
	void update_ports();
	void load_file();
};

#endif
