#include <systemc.h>
#include "memory.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

static const unsigned TEST_SIZE = 6;
static const unsigned DATA_WIDTH = 16;
sc_fifo<int> observer_memory;	
int values1[TEST_SIZE]; //vettore usato per tutti i test
int values2[TEST_SIZE]; //vettore usato per tutti i test

SC_MODULE(TestBench) 
{
	public:
	//canali per la memoria
	sc_signal<sc_lv<DATA_WIDTH> > mem_add;
	sc_signal<sc_lv<DATA_WIDTH> > mem_din;
	sc_signal<sc_lv<DATA_WIDTH> > mem_dout;
	sc_signal<sc_lv<DATA_WIDTH> > dmem_add;
	sc_signal<sc_lv<DATA_WIDTH> > dmem_din;
	sc_signal<sc_lv<DATA_WIDTH> > dmem_dout;	
	sc_signal<bool> mem_cs; //chip select
	sc_signal<bool> mem_we; //write enable
	sc_signal<bool> mem_oe; //output enable	
	sc_signal<bool> dmem_cs; //chip select
	sc_signal<bool> dmem_oe; //output enable	
	sc_signal<bool> dmem_we; //write enable
	sc_signal<unsigned> mem_file;
	sc_signal<unsigned> dmem_file; 
 
	Memory test_mem; //MEMORY 
 
  SC_CTOR(TestBench) : test_mem("test_mem")
  {
 		init_values(); 
 
		SC_THREAD(init_values_memory_test);
		test_mem.add(this->mem_add);
		test_mem.din(this->mem_din);
		test_mem.dout(this->mem_dout);
		test_mem.cs(this->mem_cs);		
		test_mem.we(this->mem_we);
		test_mem.oe(this->mem_oe);
		test_mem.file(this->mem_file);	
		SC_THREAD(observer_thread_memory);	
			sensitive << test_mem.dout;		 
	}
	
	void init_values() {
				values1[0] = 5;
				values1[1] = 4;
				values1[2] = 3;
				values1[3] = 2;
				values1[4] = 1;
				values1[5] = 0;
				
				values2[0] = 1;
				values2[1] = 3;
				values2[2] = 5;
				values2[3] = 7;
				values2[4] = 9;
				values2[5] = 0;					  		  
	} 

	void init_values_memory_test() {
		//scrittura
		mem_cs.write(1);
		mem_we.write(1);
		mem_oe.write(0);
		mem_file.write(1);
		wait(1,SC_NS);
		for (unsigned i=0;i<TEST_SIZE;i++) {
			mem_din.write(values1[i]);
			mem_add.write(i+1);       
		  wait(5,SC_NS);
		}
	
		//lettura
		mem_cs.write(1);
		mem_we.write(0);
		mem_oe.write(1);
		for (unsigned i=0;i<TEST_SIZE;i++) {
			mem_add.write(i+1);      
		  wait(5,SC_NS);
		}	
		wait(50,SC_NS);
		sc_stop();
	}

	void observer_thread_memory() {
		while(true) {
				wait();
				int value = test_mem.dout->read().to_int();
				cout << "\n\nobserver_thread: at " << sc_time_stamp() << " instruction memory out: " << test_mem.dout->read() << "\n\n";
				if (observer_memory.num_free()>0 )  observer_memory.write(value);
		} 
	}

	int check_memory() {
		int test=0;
		for (unsigned i=0;i<TEST_SIZE;i++) {
				test=observer_memory.read(); //porta fifo
		    if (test != values1[i])
		        return 1;
		}		            
		return 0;
	}

};

// ---------------------------------------------------------------------------------------
 
int sc_main(int argc, char* argv[]) {

	TestBench testbench("testbench");
	sc_start();

  return testbench.check_memory();
}
